package com.acidapps.ardei.model.category.beverage;

import com.acidapps.ardei.model.base.Category;
import com.acidapps.ardei.model.beverage.Beverage;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

@IgnoreExtraProperties
public class BeverageCategory extends Category {
    private List<Beverage> beverages;

    public BeverageCategory() {
        // Default firebase empty constructor
    }

    public BeverageCategory(int id, String name, List<Beverage> beverages) {
        super(id, name);
        this.beverages = beverages;
    }

    public List<Beverage> getBeverages() {
        return beverages;
    }
}
