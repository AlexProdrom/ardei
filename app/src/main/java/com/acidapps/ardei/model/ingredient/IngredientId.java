package com.acidapps.ardei.model.ingredient;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;

import static com.acidapps.ardei.model.ingredient.IngredientId.BASIL;
import static com.acidapps.ardei.model.ingredient.IngredientId.BEEF;
import static com.acidapps.ardei.model.ingredient.IngredientId.CHICKEN;
import static com.acidapps.ardei.model.ingredient.IngredientId.CHILLY;
import static com.acidapps.ardei.model.ingredient.IngredientId.HAM;
import static com.acidapps.ardei.model.ingredient.IngredientId.MOZZARELLA;
import static com.acidapps.ardei.model.ingredient.IngredientId.MUSHROOMS;
import static com.acidapps.ardei.model.ingredient.IngredientId.OLIVES;
import static com.acidapps.ardei.model.ingredient.IngredientId.PARMESAN;
import static com.acidapps.ardei.model.ingredient.IngredientId.PEPPERONI;
import static com.acidapps.ardei.model.ingredient.IngredientId.TOMATOES;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@IntDef({TOMATOES, BASIL, MOZZARELLA, CHILLY, PARMESAN, HAM, PEPPERONI, BEEF, OLIVES, MUSHROOMS, CHICKEN})
public @interface IngredientId {
    int TOMATOES = 0;
    int BASIL = 1;
    int MOZZARELLA = 2;
    int CHILLY = 3;
    int PARMESAN = 4;
    int HAM = 5;
    int PEPPERONI = 6;
    int BEEF = 7;
    int OLIVES = 8;
    int MUSHROOMS = 9;
    int CHICKEN = 10;
}
