package com.acidapps.ardei.model.base;

public interface ICategory {
    String getName();
}
