package com.acidapps.ardei.model.base;

public abstract class Item implements IItem {
    private String name;
    private double price;

    protected Item() {
        // Default firebase empty constructor
    }

    public Item(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String mName) {
        this.name = mName;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double mPrice) {
        this.price = mPrice;
    }
}
