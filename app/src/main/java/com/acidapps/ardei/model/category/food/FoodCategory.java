package com.acidapps.ardei.model.category.food;

import com.acidapps.ardei.model.base.Category;
import com.acidapps.ardei.model.dish.Dish;

import java.util.List;

public class FoodCategory extends Category {
    //TODO: Refactor so that IItem has getIngredients and throws exception for Beverage
    private List<Dish> dishes;

    public FoodCategory() {
        // Default firebase empty constructor
    }

    public FoodCategory(int id, String name, List<Dish> dishes) {
        super(id, name);

        this.dishes = dishes;
    }

    public List<Dish> getDishes() {
        return dishes;
    }
}
