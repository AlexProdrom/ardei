package com.acidapps.ardei.model.order;

import androidx.annotation.NonNull;

import com.acidapps.ardei.model.base.IViewType;
import com.acidapps.ardei.model.base.Item;

import java.util.Objects;

public class OrderItem implements IViewType {
    private Item item;
    private int quantity;

    public OrderItem(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    @Override
    public int getViewType() {
        return IViewType.ITEM;
    }

    public Item getItem() {
        return item;
    }

    public String getItemName() {
        return item.getName();
    }

    public int getQuantity() {
        return quantity;
    }

    public void add() {
        quantity++;
    }

    public void remove() {
        quantity--;
        if (quantity < 0) {
            quantity = 0;
        }
    }

    public double getItemPrice() {
        return item.getPrice();
    }

    public double getTotalPrice() {
        return item.getPrice() * quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equals(item.getName(), orderItem.item.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(item.getName());
    }

    @NonNull
    @Override
    public String toString() {
        return item.getName() + " x " + quantity;
    }
}
