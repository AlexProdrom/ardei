package com.acidapps.ardei.model.category.beverage;

import android.util.SparseIntArray;

import androidx.annotation.DrawableRes;

import com.acidapps.ardei.R;
import com.acidapps.ardei.model.category.food.FoodCategoryId;

import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.SOFT_DRINKS;
import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.SPIRITS;
import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.WARM_DRINKS;
import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.WINE;

public class BeverageCategoryImageHelper {
    private static SparseIntArray mBeverageCategoryImageMap = new SparseIntArray();

    static {
        mBeverageCategoryImageMap.put(SOFT_DRINKS, R.drawable.juices);
        mBeverageCategoryImageMap.put(WINE, R.drawable.wine);
        mBeverageCategoryImageMap.put(WARM_DRINKS, R.drawable.warmdrinks);
        mBeverageCategoryImageMap.put(SPIRITS, R.drawable.strongdrinks);
    }

    public static @DrawableRes int getBeverageCategoryImageDrawable(@FoodCategoryId int beverageCategoryId) {
        return mBeverageCategoryImageMap.get(beverageCategoryId);
    }
}
