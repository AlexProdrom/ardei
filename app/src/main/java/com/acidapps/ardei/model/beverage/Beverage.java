package com.acidapps.ardei.model.beverage;

import com.acidapps.ardei.model.base.Item;

public class Beverage extends Item implements IBeverage {
    public Beverage() {
        // Default firebase empty constructor
    }

    public Beverage(String name, double price) {
        super(name, price);
    }
}
