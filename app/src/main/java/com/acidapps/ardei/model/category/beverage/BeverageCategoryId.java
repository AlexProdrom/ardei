package com.acidapps.ardei.model.category.beverage;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.SOFT_DRINKS;
import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.SPIRITS;
import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.WARM_DRINKS;
import static com.acidapps.ardei.model.category.beverage.BeverageCategoryId.WINE;

@Retention(RetentionPolicy.SOURCE)
@IntDef({SOFT_DRINKS, WINE, WARM_DRINKS, SPIRITS})
public @interface BeverageCategoryId {
    int SOFT_DRINKS = 1;
    int WINE = 2;
    int WARM_DRINKS = 3;
    int SPIRITS = 4;
}

