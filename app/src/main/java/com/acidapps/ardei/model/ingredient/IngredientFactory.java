package com.acidapps.ardei.model.ingredient;

public class IngredientFactory {
    public static Ingredient createTomatoIngredient() {
        return new Ingredient(IngredientId.TOMATOES, "Tomatoes");
    }

    public static Ingredient createBasilIngredient() {
        return new Ingredient(IngredientId.BASIL, "Basil");
    }

    public static Ingredient createParmesanIngredient() {
        return new Ingredient(IngredientId.PARMESAN, "Parmesan");
    }

    public static Ingredient createBeefIngredient() {
        return new Ingredient(IngredientId.BEEF, "Beef");
    }

    public static Ingredient createMozzarellaIngredient() {
        return new Ingredient(IngredientId.MOZZARELLA, "Mozzarella");
    }

    public static Ingredient createOliveIngredient() {
        return new Ingredient(IngredientId.OLIVES, "Olives");
    }

    public static Ingredient createChillyIngredient() {
        return new Ingredient(IngredientId.CHILLY, "Chillies");
    }

    public static Ingredient createPepperoniIngredient() {
        return new Ingredient(IngredientId.PEPPERONI, "Pepperoni");
    }

    public static Ingredient createHamIngredient() {
        return new Ingredient(IngredientId.HAM, "Ham");
    }

    public static Ingredient createMushRoomIngredient() {
        return new Ingredient(IngredientId.MUSHROOMS, "Mushrooms");
    }

    public static Ingredient createChickenIngredient() {
        return new Ingredient(IngredientId.CHICKEN, "Chicken");
    }
}
