package com.acidapps.ardei.model.category.food;

import android.util.SparseIntArray;

import androidx.annotation.DrawableRes;

import com.acidapps.ardei.R;

import static com.acidapps.ardei.model.category.food.FoodCategoryId.PIZZAS;
import static com.acidapps.ardei.model.category.food.FoodCategoryId.SALADS;
import static com.acidapps.ardei.model.category.food.FoodCategoryId.STARTERS;
import static com.acidapps.ardei.model.category.food.FoodCategoryId.SWEETS;

public class FoodCategoryImageHelper {
    private static SparseIntArray foodCategoryImageMap = new SparseIntArray();

    static {
        foodCategoryImageMap.put(STARTERS, R.drawable.appertizers);
        foodCategoryImageMap.put(PIZZAS, R.drawable.pizza);
        foodCategoryImageMap.put(SALADS, R.drawable.salads);
        foodCategoryImageMap.put(SWEETS, R.drawable.dessert);
    }

    public static @DrawableRes int getFoodCategoryImageDrawable(@FoodCategoryId int foodCategoryId) {
        return foodCategoryImageMap.get(foodCategoryId);
    }
}
