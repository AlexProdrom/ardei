package com.acidapps.ardei.model.base;

public interface IItem {
    String getName();

    double getPrice();
}
