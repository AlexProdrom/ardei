package com.acidapps.ardei.model.dish;

import com.acidapps.ardei.model.base.Item;
import com.acidapps.ardei.model.ingredient.Ingredient;

import java.util.ArrayList;
import java.util.List;

public class Dish extends Item implements IDish {
    private int quantity;
    private List<Ingredient> ingredients = new ArrayList<>();

    public Dish() {
        // Default firebase empty constructor
    }

    public Dish(String name, double price, List<Ingredient> ingredients) {
        super(name, price);
        this.ingredients = ingredients;
    }

    @Override
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int mQuantity) {
        this.quantity = mQuantity;
    }

    @Override
    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getIngredientsString() {
        return ingredients.toString()
                .replace("[", "")
                .replace("]", "");
    }

    public void setIngredients(List<Ingredient> mIngredients) {
        this.ingredients = mIngredients;
    }
}