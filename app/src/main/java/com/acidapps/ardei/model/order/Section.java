package com.acidapps.ardei.model.order;

import androidx.annotation.StringRes;

import com.acidapps.ardei.model.base.IViewType;

public class Section implements IViewType {
    private @StringRes int name;

    public Section(int name) {
        this.name = name;
    }

    public @StringRes int getName() {
        return name;
    }

    @Override
    public int getViewType() {
        return IViewType.SECTION;
    }
}
