package com.acidapps.ardei.model.category.food;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.acidapps.ardei.model.category.food.FoodCategoryId.PIZZAS;
import static com.acidapps.ardei.model.category.food.FoodCategoryId.SALADS;
import static com.acidapps.ardei.model.category.food.FoodCategoryId.STARTERS;
import static com.acidapps.ardei.model.category.food.FoodCategoryId.SWEETS;

@Retention(RetentionPolicy.SOURCE)
@IntDef({STARTERS, PIZZAS, SALADS, SWEETS})
public @interface FoodCategoryId {
    int STARTERS = 1;
    int PIZZAS = 2;
    int SALADS = 3;
    int SWEETS = 4;
}
