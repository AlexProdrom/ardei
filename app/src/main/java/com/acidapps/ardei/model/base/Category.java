package com.acidapps.ardei.model.base;

public abstract class Category implements ICategory {
    private int id;
    private String name;

    public Category() {
        // Default firebase empty constructor
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }
}
