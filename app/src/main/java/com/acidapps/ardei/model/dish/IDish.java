package com.acidapps.ardei.model.dish;

import com.acidapps.ardei.model.ingredient.Ingredient;

import java.util.List;

public interface IDish {
    int getQuantity();

    List<Ingredient> getIngredients();
}
