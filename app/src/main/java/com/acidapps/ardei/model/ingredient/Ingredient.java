package com.acidapps.ardei.model.ingredient;

import androidx.annotation.NonNull;

public class Ingredient {
    private int ingredientId;
    private String name;

    public Ingredient() {
        // Default firebase empty constructor
    }

    Ingredient(@IngredientId int ingredientId, String name) {
        this.ingredientId = ingredientId;
        this.name = name;
    }

    public int getIngredientId() {
        return ingredientId;
    }

    public String getName() {
        return name;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}