package com.acidapps.ardei.model.order;

import com.acidapps.ardei.model.beverage.Beverage;
import com.acidapps.ardei.model.dish.Dish;

import java.util.ArrayList;
import java.util.List;

public class Order {
    // TODO: Make this LiveData
    private List<Dish> dishes = new ArrayList<>();
    private List<Beverage> beverages = new ArrayList<>();

    public List<Dish> getDishes() {
        return dishes;
    }

    public List<Beverage> getBeverages() {
        return beverages;
    }

    public void addDish(Dish dish) {
        dishes.add(dish);
    }

    public void addBeverage(Beverage beverage) {
        beverages.add(beverage);
    }

    public void clear() {
        dishes.clear();
        beverages.clear();
    }
}
