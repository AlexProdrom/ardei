package com.acidapps.ardei.model.base;

public interface IViewType {
    int ITEM = 0;
    int SECTION = 1;

    int getViewType();
}
