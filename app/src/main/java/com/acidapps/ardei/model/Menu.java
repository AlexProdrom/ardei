package com.acidapps.ardei.model;

import com.acidapps.ardei.model.category.beverage.BeverageCategory;
import com.acidapps.ardei.model.category.food.FoodCategory;

import java.util.List;

public class Menu {
    private List<FoodCategory> foodCategories;
    private List<BeverageCategory> beverageCategories;

    public Menu() {
        // Default empty constructor for Firebase
    }

    public Menu(List<FoodCategory> foodCategories, List<BeverageCategory> beverageCategories) {
        this.foodCategories = foodCategories;
        this.beverageCategories = beverageCategories;
    }

    public List<FoodCategory> getFoodCategories() {
        return foodCategories;
    }

    public List<BeverageCategory> getBeverageCategories() {
        return beverageCategories;
    }
}