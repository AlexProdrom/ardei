package com.acidapps.ardei.data.oracle.model

import com.google.gson.annotations.Expose

data class OrderOverview(
        @Expose var id: Int?,
        @Expose var tablenr: Int?,
        @Expose var items: String?,
        @Expose var price: Double?,
        @Expose var date: String?
)