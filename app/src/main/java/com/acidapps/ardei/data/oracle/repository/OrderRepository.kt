package com.acidapps.ardei.data.oracle.repository


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.acidapps.ardei.data.oracle.ApiService
import com.acidapps.ardei.data.oracle.ApiUtils
import com.acidapps.ardei.data.oracle.model.PlaceOrderResponse
import com.acidapps.ardei.data.oracle.model.RetrieveOrdersResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrderRepository : IPlaceOrderRepository, IRetrieveOrdersRepository {
    private val apiService: ApiService = ApiUtils.getApiService()

    private var placeOrderResponseMutableLiveData = MutableLiveData<PlaceOrderResponse>()
    private var retrieveOrdersResponseMutableLiveData = MutableLiveData<RetrieveOrdersResponse>()

    override fun sendOrder(tableNr: Int, items: String, price: Double, date: String) {
        apiService
                .sendOrder(tableNr, items, price, date)
                .enqueue(object : Callback<PlaceOrderResponse> {
                    override fun onResponse(call: Call<PlaceOrderResponse>, response: Response<PlaceOrderResponse>) {
                        if (response.body() != null) {
                            placeOrderResponseMutableLiveData.value = response.body()
                            placeOrderResponseMutableLiveData = MutableLiveData()
                        }
                    }

                    override fun onFailure(call: Call<PlaceOrderResponse>, t: Throwable) {

                    }
                })
    }

    override fun retrieveOrders() {
        apiService
                .orders
                .enqueue(object : Callback<RetrieveOrdersResponse> {
                    override fun onResponse(call: Call<RetrieveOrdersResponse>, response: Response<RetrieveOrdersResponse>) {
                        retrieveOrdersResponseMutableLiveData.value = response.body()
                        retrieveOrdersResponseMutableLiveData = MutableLiveData()
                    }

                    override fun onFailure(call: Call<RetrieveOrdersResponse>, t: Throwable) {}
                })
    }

    override fun getPlaceOrderResponseMutableLiveData(): LiveData<PlaceOrderResponse> {
        return placeOrderResponseMutableLiveData
    }

    override fun getRetrieveOrdersResponseMutableLiveData(): MutableLiveData<RetrieveOrdersResponse> {
        return retrieveOrdersResponseMutableLiveData
    }
}
