package com.acidapps.ardei.data.oracle.repository

import androidx.lifecycle.LiveData
import com.acidapps.ardei.data.oracle.model.PlaceOrderResponse


interface IPlaceOrderRepository {
    fun sendOrder(tableNr: Int, items: String, price: Double, date: String)

    fun getPlaceOrderResponseMutableLiveData(): LiveData<PlaceOrderResponse>
}