package com.acidapps.ardei.data.oracle.model

import com.google.gson.annotations.Expose

class PlaceOrderResponse(
        @Expose var error: Boolean?,
        @Expose var message: String?
)