package com.acidapps.ardei.data.oracle.repository

import androidx.lifecycle.MutableLiveData
import com.acidapps.ardei.data.oracle.model.RetrieveOrdersResponse


interface IRetrieveOrdersRepository {
    fun retrieveOrders()

    fun getRetrieveOrdersResponseMutableLiveData(): MutableLiveData<RetrieveOrdersResponse>
}
