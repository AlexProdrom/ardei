package com.acidapps.ardei.data.oracle;

import com.acidapps.ardei.data.oracle.model.PlaceOrderResponse;
import com.acidapps.ardei.data.oracle.model.RetrieveOrdersResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {
    @POST("Ardei/v1/Api.php?apicall=create-order")
    @FormUrlEncoded
    Call<PlaceOrderResponse> sendOrder(@Field("tablenr") int title,
                                       @Field("items") String body,
                                       @Field("price") double userId,
                                       @Field("date") String date);

    @GET("Ardei/v1/Api.php?apicall=orders")
    Call<RetrieveOrdersResponse> getOrders();
}
