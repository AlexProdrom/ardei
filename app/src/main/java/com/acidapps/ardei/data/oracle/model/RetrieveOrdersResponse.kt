package com.acidapps.ardei.data.oracle.model

import com.google.gson.annotations.Expose

class RetrieveOrdersResponse (
    @Expose var error: Boolean?,
    @Expose var message: String?,
    @Expose var orders: List<OrderOverview>?
)