package com.acidapps.ardei.data.oracle;

public class ApiUtils {
    private ApiUtils() {
    }

    // TODO: Insert your ip address
    private static final String BASE_URL = "http://192.168.178.13/";

    public static ApiService getApiService() {
        return RetrofitClient.getClient(BASE_URL).create(ApiService.class);
    }
}
