package com.acidapps.ardei.data.firebase

import android.util.Log
import androidx.lifecycle.LiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener

class FirebaseQueryLiveData(private val databaseReference: DatabaseReference) : LiveData<DataSnapshot>() {
    private val TAG = FirebaseQueryLiveData::class.java.simpleName

    private val menuEventListener = MenuEventListener()

    override fun onActive() {
        Log.d(TAG, "onActive")
        databaseReference.addListenerForSingleValueEvent(menuEventListener)
    }

    override fun onInactive() {
        Log.d(TAG, "onInactive")
        databaseReference.removeEventListener(menuEventListener)
    }

    private inner class MenuEventListener : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            value = dataSnapshot
        }

        override fun onCancelled(databaseError: DatabaseError) {
            Log.e(TAG, "Can't listen to databaseReference $databaseReference", databaseError.toException())
        }
    }
}