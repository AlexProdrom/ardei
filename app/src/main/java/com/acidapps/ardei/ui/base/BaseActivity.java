package com.acidapps.ardei.ui.base;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.acidapps.ardei.R;
import com.acidapps.ardei.data.oracle.repository.OrderRepository;
import com.acidapps.ardei.model.order.Order;

import java.util.Optional;

public abstract class BaseActivity extends AppCompatActivity implements IActivityView {
    private TextView actionBarTitle;
    private Order order;
    private OrderRepository orderRepository;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Optional<ActionBar> actionBar = Optional.ofNullable(getSupportActionBar());
        actionBar.ifPresent(ab -> {
            ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            ab.setCustomView(R.layout.actionbar);
        });

        actionBarTitle = findViewById(R.id.title);
        init();
    }

    private void init() {
        order = new Order();
        orderRepository = new OrderRepository();
    }

    @Override
    public void setTitle(@StringRes int title) {
        actionBarTitle.setText(title);
    }

    @Override
    public void setTitle(String title) {
        actionBarTitle.setText(title);
    }

    @Override
    public Order getOrder() {
        return order;
    }

    @Override
    public OrderRepository getOrderRepository() {
        return orderRepository;
    }
}
