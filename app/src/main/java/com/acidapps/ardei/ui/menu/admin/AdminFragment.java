package com.acidapps.ardei.ui.menu.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentAdminBinding;
import com.acidapps.ardei.ui.base.BaseFragment;
import com.acidapps.ardei.ui.menu.MenuActivity;
import com.acidapps.ardei.ui.menu.orderoverview.OrderOverviewFragment;

public class AdminFragment extends BaseFragment {
    public static final String TAG = AdminFragment.class.getSimpleName();

    private AdminViewModel adminViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        FragmentAdminBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_admin, container, false);
        initTitle(R.string.title_admin);

        adminViewModel.isValidLiveData().observe(this, isValid -> {
            if (isValid) {
                ((MenuActivity) requireActivity()).navigateToFragment(new OrderOverviewFragment());
                ((MenuActivity) requireActivity()).login();
            } else {
                showFailedLoginMessage();
            }
        });

        binding.btnLogin.setOnClickListener(v -> adminViewModel.validatePassword(
                binding.inputNameText.getText().toString(),
                binding.inputPasswordText.getText().toString()
        ));
        return binding.getRoot();
    }

    @Override
    protected void initViewModel() {
        adminViewModel = ViewModelProviders.of(this).get(AdminViewModel.class);
    }

    private void showFailedLoginMessage() {
        Toast.makeText(getContext(), "Wrong name or password", Toast.LENGTH_SHORT).show();
    }
}
