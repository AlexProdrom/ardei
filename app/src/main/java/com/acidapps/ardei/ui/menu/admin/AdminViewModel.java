package com.acidapps.ardei.ui.menu.admin;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AdminViewModel extends ViewModel {
    private MutableLiveData<Boolean> isValidLiveData = new MutableLiveData<>();

    void validatePassword(String name, String password) {
        isValidLiveData.setValue(name.equals("Admin") && password.equals("lepra"));
    }

    LiveData<Boolean> isValidLiveData() {
        return isValidLiveData;
    }
}
