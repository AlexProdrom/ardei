package com.acidapps.ardei.ui.menu.order.adapter;

import android.util.SparseArray;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.model.order.Section;
import com.acidapps.ardei.model.base.IViewType;
import com.acidapps.ardei.model.order.OrderItem;
import com.acidapps.ardei.ui.base.IViewTypeDelegateAdapter;
import com.acidapps.ardei.ui.menu.order.OnChangeQuantityCallback;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<IViewType> items = new ArrayList<>();
    private SparseArray<IViewTypeDelegateAdapter> delegateAdapters = new SparseArray<>();

    public OrderAdapter(OnChangeQuantityCallback onChangeQuantityCallback) {
        delegateAdapters.put(IViewType.ITEM, new ItemDelegateAdapter(onChangeQuantityCallback));
        delegateAdapters.put(IViewType.SECTION, new SectionDelegateAdapter());
    }

    public void addDishes(List<OrderItem> dishes) {
        items.clear();
        items.add(new Section(R.string.title_food));
        items.addAll(dishes);
    }

    public void addBeverages(List<OrderItem> beverages) {
        items.add(new Section(R.string.title_beverage));
        items.addAll(beverages);
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
