package com.acidapps.ardei.ui.menu.orderoverview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.acidapps.ardei.data.oracle.model.RetrieveOrdersResponse;
import com.acidapps.ardei.data.oracle.repository.IRetrieveOrdersRepository;

class OrderOverviewViewModel extends ViewModel {
    private IRetrieveOrdersRepository retrieveOrdersRepository;

    OrderOverviewViewModel(IRetrieveOrdersRepository orderRepository) {
        retrieveOrdersRepository = orderRepository;
    }

    void retrieveOrders() {
        retrieveOrdersRepository.retrieveOrders();
    }

    LiveData<RetrieveOrdersResponse> getRetrieveOrdersLiveData() {
        return retrieveOrdersRepository.getRetrieveOrdersResponseMutableLiveData();
    }
}
