package com.acidapps.ardei.ui.menu.orderoverview;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.acidapps.ardei.data.oracle.repository.IRetrieveOrdersRepository;
import com.acidapps.ardei.data.oracle.repository.OrderRepository;

public class OrderOverviewViewModelFactory implements ViewModelProvider.Factory {
    private IRetrieveOrdersRepository retrieveOrdersRepository;

    public OrderOverviewViewModelFactory(OrderRepository orderRepository) {
        this.retrieveOrdersRepository = orderRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OrderOverviewViewModel(retrieveOrdersRepository);
    }
}
