package com.acidapps.ardei.ui.menu.food.foodcategorylist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FoodCategoryItemBinding;
import com.acidapps.ardei.model.base.Category;
import com.acidapps.ardei.model.category.food.FoodCategory;

import java.util.ArrayList;
import java.util.List;

public class FoodCategoryAdapter extends RecyclerView.Adapter<FoodCategoryAdapter.CategoryViewHolder> {
    private OnFoodCategoryClickCallback mOnFoodCategoryClickCallback;
    private List<? extends Category> mCategories = new ArrayList<>();

    FoodCategoryAdapter(OnFoodCategoryClickCallback OnFoodCategoryClickCallback) {
        mOnFoodCategoryClickCallback = OnFoodCategoryClickCallback;
    }

    void setCategoryList(final List<? extends Category> categories) {
        if (mCategories.isEmpty()) {
            mCategories = categories;
            notifyItemRangeInserted(0, categories.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {

                @Override
                public int getOldListSize() {
                    return mCategories.size();
                }

                @Override
                public int getNewListSize() {
                    return categories.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    Category oldCategory = mCategories.get(oldItemPosition);
                    Category newCategory = mCategories.get(newItemPosition);

                    return oldCategory.getName().equals(newCategory.getName());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return false;
                }
            });
            mCategories = categories;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FoodCategoryItemBinding categoryItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.food_category_item,
                parent,
                false);
        categoryItemBinding.setClickCallback(mOnFoodCategoryClickCallback);
        return new CategoryViewHolder(categoryItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.mBinding.setCategory((FoodCategory) mCategories.get(position));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        final FoodCategoryItemBinding mBinding;

        CategoryViewHolder(FoodCategoryItemBinding categoryItemBinding) {
            super(categoryItemBinding.getRoot());
            mBinding = categoryItemBinding;
        }
    }
}
