package com.acidapps.ardei.ui.menu.food.foodcategorylist;

import com.acidapps.ardei.model.category.food.FoodCategory;

public interface OnFoodCategoryClickCallback {
    void onClick(FoodCategory category);
}
