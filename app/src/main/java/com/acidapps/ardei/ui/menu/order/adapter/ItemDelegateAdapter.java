package com.acidapps.ardei.ui.menu.order.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.OrderItemBinding;
import com.acidapps.ardei.model.base.IViewType;
import com.acidapps.ardei.model.order.OrderItem;
import com.acidapps.ardei.ui.base.IViewTypeDelegateAdapter;
import com.acidapps.ardei.ui.menu.order.OnChangeQuantityCallback;

public class ItemDelegateAdapter implements IViewTypeDelegateAdapter {
    private OnChangeQuantityCallback onChangeQuantityCallback;

    ItemDelegateAdapter(OnChangeQuantityCallback onChangeQuantityCallback) {
        this.onChangeQuantityCallback = onChangeQuantityCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        OrderItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.order_item,
                parent,
                false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, IViewType viewType) {
        OrderItem item = (OrderItem) viewType;

        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.binding.setOrderItem(item);
        itemViewHolder.binding.setChangeQuantityCallback(onChangeQuantityCallback);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        final OrderItemBinding binding;

        ItemViewHolder(@NonNull OrderItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
