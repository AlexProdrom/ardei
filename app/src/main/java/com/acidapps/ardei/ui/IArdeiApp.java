package com.acidapps.ardei.ui;

import com.acidapps.ardei.model.category.beverage.BeverageCategory;
import com.acidapps.ardei.model.category.food.FoodCategory;

import java.util.List;

public interface IArdeiApp {
    List<FoodCategory> getFoodCategories();

    List<BeverageCategory> getBeverageCategories();
}
