package com.acidapps.ardei.ui.menu.beverage.beveragelist;

import com.acidapps.ardei.model.beverage.Beverage;

public interface OnBeverageAddedCallback {
    void onAdd(Beverage beverage);
}
