package com.acidapps.ardei.ui.menu.beverage.beveragecategorylist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.BeverageCategoryItemBinding;
import com.acidapps.ardei.model.base.Category;
import com.acidapps.ardei.model.category.beverage.BeverageCategory;

import java.util.ArrayList;
import java.util.List;

public class BeverageCategoryAdapter extends RecyclerView.Adapter<BeverageCategoryAdapter.CategoryViewHolder> {
    private OnBeverageCategoryClickCallback mOnBeverageCategoryClickCallback;
    private List<? extends Category> mCategories = new ArrayList<>();

    BeverageCategoryAdapter(OnBeverageCategoryClickCallback OnBeverageCategoryClickCallback) {
        mOnBeverageCategoryClickCallback = OnBeverageCategoryClickCallback;
    }

    void setCategoryList(final List<? extends Category> categories) {
        if (mCategories.isEmpty()) {
            mCategories = categories;
            notifyItemRangeInserted(0, categories.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {

                @Override
                public int getOldListSize() {
                    return mCategories.size();
                }

                @Override
                public int getNewListSize() {
                    return categories.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    Category oldCategory = mCategories.get(oldItemPosition);
                    Category newCategory = mCategories.get(newItemPosition);

                    return oldCategory.getName().equals(newCategory.getName());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return false;
                }
            });
            mCategories = categories;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public BeverageCategoryAdapter.CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BeverageCategoryItemBinding categoryItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.beverage_category_item,
                parent,
                false);
        categoryItemBinding.setClickCallback(mOnBeverageCategoryClickCallback);
        return new BeverageCategoryAdapter.CategoryViewHolder(categoryItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BeverageCategoryAdapter.CategoryViewHolder holder, int position) {
        holder.mBinding.setCategory((BeverageCategory) mCategories.get(position));
        holder.mBinding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {
        final BeverageCategoryItemBinding mBinding;

        CategoryViewHolder(BeverageCategoryItemBinding categoryItemBinding) {
            super(categoryItemBinding.getRoot());
            mBinding = categoryItemBinding;
        }
    }
}
