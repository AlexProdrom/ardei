package com.acidapps.ardei.ui.menu.orderoverview.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.data.oracle.model.OrderOverview;
import com.acidapps.ardei.databinding.OrderOverviewItemBinding;

import java.util.ArrayList;
import java.util.List;

public class OrderOverviewAdapter extends RecyclerView.Adapter<OrderOverviewAdapter.OrderOverviewViewHolder> {
    private List<OrderOverview> orderOverviews = new ArrayList<>();

    public void setOrderOverviews(final List<OrderOverview> orderOverviews) {
        if (orderOverviews.isEmpty()) {
            this.orderOverviews = orderOverviews;
            notifyItemRangeInserted(0, orderOverviews.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {

                @Override
                public int getOldListSize() {
                    return OrderOverviewAdapter.this.orderOverviews.size();
                }

                @Override
                public int getNewListSize() {
                    return orderOverviews.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    OrderOverview oldOrderOverview = OrderOverviewAdapter.this.orderOverviews.get(oldItemPosition);
                    OrderOverview newOrderOverview = orderOverviews.get(newItemPosition);

                    return oldOrderOverview.getId().equals(newOrderOverview.getId());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return false;
                }
            });
            this.orderOverviews = orderOverviews;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public OrderOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        OrderOverviewItemBinding orderOverviewItemBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.order_overview_item,
                parent,
                false);
        return new OrderOverviewAdapter.OrderOverviewViewHolder(orderOverviewItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderOverviewViewHolder holder, int position) {
        holder.binding.setOrderOverview(orderOverviews.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return orderOverviews.size();
    }

    class OrderOverviewViewHolder extends RecyclerView.ViewHolder {
        final OrderOverviewItemBinding binding;

        OrderOverviewViewHolder(OrderOverviewItemBinding orderOverviewItemBinding) {
            super(orderOverviewItemBinding.getRoot());
            binding = orderOverviewItemBinding;
        }
    }
}
