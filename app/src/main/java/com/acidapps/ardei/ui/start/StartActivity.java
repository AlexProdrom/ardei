package com.acidapps.ardei.ui.start;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.ActivityStartBinding;
import com.acidapps.ardei.ui.menu.MenuActivity;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStartBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_start);

        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
        binding.tvSplashScreenTitle.startAnimation(animation1);

        StartViewModel startViewModel = ViewModelProviders.of(this).get(StartViewModel.class);
        startViewModel.getMenuLiveData().observe(this, menu -> {
            Intent menuActivity = new Intent(StartActivity.this, MenuActivity.class);
            startActivity(menuActivity);
            finish();
        });
    }
}
