package com.acidapps.ardei.ui.menu.food.foodlist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentDishListBinding;
import com.acidapps.ardei.ui.ArdeiApp;
import com.acidapps.ardei.ui.base.BaseActivity;
import com.acidapps.ardei.ui.base.BaseFragment;

public class DishListFragment extends BaseFragment {
    private static final String CATEGORY_NAME_KEY = "dish_category_name";

    private DishListViewModel foodCategoryListViewModel;

    public static DishListFragment newInstance(String categoryName) {
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_NAME_KEY, categoryName);
        DishListFragment fragment = new DishListFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentDishListBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dish_list, container, false);

        DishAdapter mDishAdapter = new DishAdapter(foodCategoryListViewModel);
        if (getArguments() != null) {
            String categoryName = getArguments().getString(CATEGORY_NAME_KEY);
            mDishAdapter.setDishes(foodCategoryListViewModel.getDishes(categoryName));
            initTitle(categoryName);
        }

        mBinding.dishList.setAdapter(mDishAdapter);
        mBinding.dishList.setLayoutManager(new LinearLayoutManager(getContext()));
        return mBinding.getRoot();
    }

    @Override
    protected void initViewModel() {
        BaseActivity baseActivity = (BaseActivity) requireActivity();
        DishListViewModelFactory dishListViewModelFactory = new DishListViewModelFactory(
                (ArdeiApp) baseActivity.getApplication(),
                baseActivity.getOrder());
        foodCategoryListViewModel = ViewModelProviders
                .of(this, dishListViewModelFactory)
                .get(DishListViewModel.class);
    }
}