package com.acidapps.ardei.ui.base;

import androidx.annotation.StringRes;

import com.acidapps.ardei.data.oracle.repository.OrderRepository;
import com.acidapps.ardei.model.order.Order;

public interface IActivityView {
    void setTitle(@StringRes int title);

    void setTitle(String title);

    Order getOrder();

    OrderRepository getOrderRepository();
}
