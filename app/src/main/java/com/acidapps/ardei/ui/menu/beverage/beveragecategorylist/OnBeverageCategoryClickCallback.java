package com.acidapps.ardei.ui.menu.beverage.beveragecategorylist;

import com.acidapps.ardei.model.category.beverage.BeverageCategory;

public interface OnBeverageCategoryClickCallback {
    void onClick(BeverageCategory category);

}
