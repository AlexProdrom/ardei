package com.acidapps.ardei.ui.menu.beverage.beveragelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentDishListBinding;
import com.acidapps.ardei.ui.ArdeiApp;
import com.acidapps.ardei.ui.base.BaseActivity;
import com.acidapps.ardei.ui.base.BaseFragment;

public class BeverageListFragment extends BaseFragment {
    private static final String CATEGORY_NAME_KEY = "dish_category_name";

    private BeverageListViewModel beverageCategoryListViewModel;

    public static BeverageListFragment newInstance(String categoryName) {
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_NAME_KEY, categoryName);
        BeverageListFragment fragment = new BeverageListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentDishListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dish_list, container, false);

        BeverageAdapter dishAdapter = new BeverageAdapter(beverageCategoryListViewModel);
        if (getArguments() != null) {
            String categoryName = getArguments().getString(CATEGORY_NAME_KEY);
            dishAdapter.setBeverages(beverageCategoryListViewModel.getBeverages(categoryName));
            initTitle(categoryName);
        }

        binding.dishList.setAdapter(dishAdapter);
        binding.dishList.setLayoutManager(new LinearLayoutManager(getContext()));
        return binding.getRoot();
    }

    @Override
    protected void initViewModel() {
        BaseActivity baseActivity = (BaseActivity) requireActivity();
        BeverageListViewModelFactory beverageListViewModelFactory = new BeverageListViewModelFactory(
                (ArdeiApp) baseActivity.getApplication(),
                baseActivity.getOrder());
        beverageCategoryListViewModel = ViewModelProviders
                .of(this, beverageListViewModelFactory)
                .get(BeverageListViewModel.class);
    }
}
