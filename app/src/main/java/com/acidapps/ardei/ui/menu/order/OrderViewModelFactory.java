package com.acidapps.ardei.ui.menu.order;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.acidapps.ardei.data.oracle.repository.IPlaceOrderRepository;
import com.acidapps.ardei.data.oracle.repository.OrderRepository;
import com.acidapps.ardei.model.order.Order;

public class OrderViewModelFactory implements ViewModelProvider.Factory {
    private Order order;
    private IPlaceOrderRepository placeOrderRepository;

    OrderViewModelFactory(Order order, OrderRepository orderRepository) {
        this.order = order;
        this.placeOrderRepository = orderRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new OrderViewModel(order, placeOrderRepository);
    }
}
