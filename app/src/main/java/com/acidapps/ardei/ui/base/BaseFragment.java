package com.acidapps.ardei.ui.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViewModel();
    }

    protected abstract void initViewModel();

    protected void initTitle(@StringRes int title) {
        ((IActivityView) requireActivity()).setTitle(title);
    }

    protected void initTitle(String title) {
        ((IActivityView) requireActivity()).setTitle(title);
    }
}
