package com.acidapps.ardei.ui.menu.food.foodcategorylist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentCategoryListBinding;
import com.acidapps.ardei.ui.base.BaseFragment;
import com.acidapps.ardei.ui.menu.MenuActivity;
import com.acidapps.ardei.ui.menu.food.foodlist.DishListFragment;

public class FoodCategoryListFragment extends BaseFragment {
    private FragmentCategoryListBinding binding;
    private FoodCategoryListViewModel foodCategoryListViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FoodCategoryAdapter foodCategoryAdapter = createAdapter();
        binding.categoryList.setAdapter(foodCategoryAdapter);
        binding.categoryList.setLayoutManager(new LinearLayoutManager(getContext()));
        initTitle(R.string.title_food);
    }

    @Override
    protected void initViewModel() {
        foodCategoryListViewModel = ViewModelProviders.of(this).get(FoodCategoryListViewModel.class);
    }

    private FoodCategoryAdapter createAdapter() {
        FoodCategoryAdapter foodCategoryAdapter = new FoodCategoryAdapter(category -> {
            DishListFragment dishListFragment = DishListFragment.newInstance(category.getName());
            ((MenuActivity) requireActivity()).navigateToFragmentBackStack(dishListFragment);
        });
        foodCategoryAdapter.setCategoryList(foodCategoryListViewModel.getFoodCategories());
        return foodCategoryAdapter;
    }
}
