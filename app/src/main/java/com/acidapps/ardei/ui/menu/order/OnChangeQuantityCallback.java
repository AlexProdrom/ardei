package com.acidapps.ardei.ui.menu.order;

public interface OnChangeQuantityCallback {
    void onAdd(String itemName);

    void onRemove(String itemName);
}
