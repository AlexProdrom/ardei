package com.acidapps.ardei.ui.menu.order.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.model.order.Section;
import com.acidapps.ardei.model.base.IViewType;
import com.acidapps.ardei.ui.base.IViewTypeDelegateAdapter;

public class SectionDelegateAdapter implements IViewTypeDelegateAdapter {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.section_item,
                        parent,
                        false);
        return new SectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, IViewType viewType) {
        Section section = (Section) viewType;
        ((SectionViewHolder) holder).setSectionText(section.getName());
    }

    class SectionViewHolder extends RecyclerView.ViewHolder {
        private TextView sectionTextView;

        SectionViewHolder(@NonNull View itemView) {
            super(itemView);
            sectionTextView = itemView.findViewById(R.id.tv_section);
        }

        void setSectionText(@StringRes int text) {
            sectionTextView.setText(text);
        }
    }
}
