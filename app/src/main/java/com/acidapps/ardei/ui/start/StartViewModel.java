package com.acidapps.ardei.ui.start;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MediatorLiveData;

import com.acidapps.ardei.data.firebase.FirebaseQueryLiveData;
import com.acidapps.ardei.ui.ArdeiApp;
import com.acidapps.ardei.model.Menu;
import com.google.firebase.database.FirebaseDatabase;

public class StartViewModel extends AndroidViewModel {
    private MediatorLiveData<Menu> mMenuLiveData = new MediatorLiveData<>();

    public StartViewModel(@NonNull Application application) {
        super(application);

        FirebaseQueryLiveData mLiveData = new FirebaseQueryLiveData(FirebaseDatabase.getInstance().getReference());
        mMenuLiveData.addSource(mLiveData, dataSnapshot -> {
            Menu menu = dataSnapshot.getValue(Menu.class);
            ((ArdeiApp) getApplication()).setMenu(menu);
            mMenuLiveData.postValue(menu);
        });
    }

    MediatorLiveData<Menu> getMenuLiveData() {
        return mMenuLiveData;
    }
}