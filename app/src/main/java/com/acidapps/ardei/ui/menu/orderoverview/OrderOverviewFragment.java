package com.acidapps.ardei.ui.menu.orderoverview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentOrderOverviewBinding;
import com.acidapps.ardei.ui.base.BaseActivity;
import com.acidapps.ardei.ui.base.BaseFragment;
import com.acidapps.ardei.ui.menu.orderoverview.adapter.OrderOverviewAdapter;

public class OrderOverviewFragment extends BaseFragment {
    public static final String TAG = OrderOverviewFragment.class.getSimpleName();

    private OrderOverviewAdapter orderOverviewAdapter;
    private OrderOverviewViewModel orderOverviewViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        FragmentOrderOverviewBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_overview, container, false);

        orderOverviewAdapter = new OrderOverviewAdapter();
        orderOverviewViewModel
                .getRetrieveOrdersLiveData()
                .observe(this, retrieveOrdersResponse -> {
                    orderOverviewAdapter.setOrderOverviews(retrieveOrdersResponse.getOrders());
                    orderOverviewAdapter.notifyDataSetChanged();
                });
        orderOverviewViewModel.retrieveOrders();

        binding.orderOverviewList.setAdapter(orderOverviewAdapter);
        binding.orderOverviewList.setLayoutManager(new LinearLayoutManager(getContext()));
        initTitle(R.string.title_order_overview);
        return binding.getRoot();
    }

    @Override
    protected void initViewModel() {
        BaseActivity baseActivity = (BaseActivity) requireActivity();
        OrderOverviewViewModelFactory orderOverviewViewModelFactory = new OrderOverviewViewModelFactory(baseActivity.getOrderRepository());
        orderOverviewViewModel = ViewModelProviders.of(this, orderOverviewViewModelFactory).get(OrderOverviewViewModel.class);
    }
}
