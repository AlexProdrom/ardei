package com.acidapps.ardei.ui.menu.order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentOrderBinding;
import com.acidapps.ardei.ui.base.BaseActivity;
import com.acidapps.ardei.ui.base.BaseFragment;
import com.acidapps.ardei.ui.menu.order.adapter.OrderAdapter;

public class OrderFragment extends BaseFragment {
    private OrderAdapter orderAdapter;
    private OrderViewModel orderViewModel;
    private FragmentOrderBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        orderAdapter = new OrderAdapter(orderViewModel);
        orderViewModel.getFoodOrderItems().observe(this, foodOrderItems -> {
            if (!foodOrderItems.isEmpty()) {
                orderAdapter.addDishes(foodOrderItems);
            }
            orderAdapter.notifyDataSetChanged();
        });

        orderViewModel.getBeverageOrderItem().observe(this, beverageBeverageItems -> {
            if (!beverageBeverageItems.isEmpty()) {
                orderAdapter.addBeverages(beverageBeverageItems);
            }
            orderAdapter.notifyDataSetChanged();
        });


        binding.setOrderViewModel(orderViewModel);
        binding.orderList.setAdapter(orderAdapter);
        binding.orderList.setLayoutManager(new LinearLayoutManager(getContext()));
        initTitle(R.string.title_order);

        orderViewModel.getPlaceOrderResponseMutableLiveData().observe(this, placeOrderResponse -> {
            Toast.makeText(getContext(), placeOrderResponse.getMessage(), Toast.LENGTH_SHORT).show();
            orderViewModel.clearOrder();
            orderAdapter.clear();
        });
    }

    @Override
    protected void initViewModel() {
        BaseActivity baseActivity = (BaseActivity) getActivity();
        OrderViewModelFactory orderViewModelFactory = new OrderViewModelFactory(baseActivity.getOrder(), baseActivity.getOrderRepository());
        orderViewModel = ViewModelProviders
                .of(this, orderViewModelFactory)
                .get(OrderViewModel.class);
    }
}
