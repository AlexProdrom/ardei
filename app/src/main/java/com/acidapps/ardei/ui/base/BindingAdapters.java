package com.acidapps.ardei.ui.base;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.databinding.BindingAdapter;

import com.acidapps.ardei.model.category.beverage.BeverageCategoryImageHelper;
import com.acidapps.ardei.model.category.food.FoodCategoryImageHelper;

public class BindingAdapters {
    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("foodImageUrl")
    public static void loadFoodImage(ImageView view, @DrawableRes int drawableRes) {
        view.setBackgroundResource(FoodCategoryImageHelper.getFoodCategoryImageDrawable(drawableRes));
    }

    @BindingAdapter("beverageImageUrl")
    public static void loadBeverageImage(ImageView view, @DrawableRes int drawableRes) {
        view.setBackgroundResource(BeverageCategoryImageHelper.getBeverageCategoryImageDrawable(drawableRes));
    }
}
