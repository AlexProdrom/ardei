package com.acidapps.ardei.ui.menu.beverage.beveragecategorylist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.FragmentCategoryListBinding;
import com.acidapps.ardei.ui.base.BaseFragment;
import com.acidapps.ardei.ui.menu.MenuActivity;
import com.acidapps.ardei.ui.menu.beverage.beveragelist.BeverageListFragment;

public class BeverageCategoryListFragment extends BaseFragment {
    private BeverageCategoryListViewModel beverageCategoryListViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        FragmentCategoryListBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_list, container, false);

        BeverageCategoryAdapter beverageCategoryAdapter = createAdapter();
        mBinding.categoryList.setAdapter(beverageCategoryAdapter);
        mBinding.categoryList.setLayoutManager(new LinearLayoutManager(getContext()));

        initTitle(R.string.title_beverage);
        return mBinding.getRoot();
    }

    @Override
    protected void initViewModel() {
        beverageCategoryListViewModel = ViewModelProviders.of(this).get(BeverageCategoryListViewModel.class);
    }

    private BeverageCategoryAdapter createAdapter() {
        BeverageCategoryAdapter beverageCategoryAdapter = new BeverageCategoryAdapter(
                category -> {
                    BeverageListFragment dishListFragment = BeverageListFragment.newInstance(category.getName());
                    ((MenuActivity) requireActivity()).navigateToFragmentBackStack(dishListFragment);
                });
        beverageCategoryAdapter.setCategoryList(beverageCategoryListViewModel.getBeverageCategories());
        return beverageCategoryAdapter;
    }
}
