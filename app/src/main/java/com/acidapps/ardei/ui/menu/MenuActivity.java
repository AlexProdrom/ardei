package com.acidapps.ardei.ui.menu;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.ActivityMenuBinding;
import com.acidapps.ardei.ui.base.BaseActivity;
import com.acidapps.ardei.ui.menu.admin.AdminFragment;
import com.acidapps.ardei.ui.menu.beverage.beveragecategorylist.BeverageCategoryListFragment;
import com.acidapps.ardei.ui.menu.food.foodcategorylist.FoodCategoryListFragment;
import com.acidapps.ardei.ui.menu.order.OrderFragment;
import com.acidapps.ardei.ui.menu.orderoverview.OrderOverviewFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MenuActivity extends BaseActivity {
    private static final String TAG = MenuActivity.class.getSimpleName();

    private Fragment fragment;
    private boolean login = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_food:
                navigateToFragment(new FoodCategoryListFragment());
                return true;
            case R.id.navigation_beverage:
                navigateToFragment(new BeverageCategoryListFragment());
                return true;
            case R.id.navigation_order:
                navigateToFragment(new OrderFragment());
                return true;
            case R.id.navigation_admin:
                item.setChecked(true);
                fragment = !login ? new AdminFragment() : new OrderOverviewFragment();
                navigateToFragment(fragment);
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMenuBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_menu);
        binding.bnvFlows.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            navigateToFragment(new FoodCategoryListFragment());
        }
    }

    public void navigateToFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment,
                        fragment,
                        TAG)
                .commit();
    }

    public void navigateToFragmentBackStack(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment,
                        fragment,
                        TAG)
                .addToBackStack(null)
                .commit();
    }

    public void login() {
        login = true;
    }
}
