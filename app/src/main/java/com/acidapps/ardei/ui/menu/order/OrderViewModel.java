package com.acidapps.ardei.ui.menu.order;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.acidapps.ardei.data.oracle.model.PlaceOrderResponse;
import com.acidapps.ardei.data.oracle.repository.IPlaceOrderRepository;
import com.acidapps.ardei.model.base.Item;
import com.acidapps.ardei.model.order.Order;
import com.acidapps.ardei.model.order.OrderItem;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public class OrderViewModel extends ViewModel implements OnChangeQuantityCallback {
    private Order order;
    private IPlaceOrderRepository placeOrderRepository;

    private List<OrderItem> foodOrderItems;
    private List<OrderItem> beverageOrderItems;

    private MutableLiveData<List<OrderItem>> dishesLiveData = new MutableLiveData<>();
    private MutableLiveData<List<OrderItem>> beveragesLiveData = new MutableLiveData<>();

    OrderViewModel(Order order, IPlaceOrderRepository placeOrderRepository) {
        this.order = order;
        this.placeOrderRepository = placeOrderRepository;

        foodOrderItems = convertToOrderItem(order.getDishes());
        beverageOrderItems = convertToOrderItem(order.getBeverages());

        dishesLiveData.setValue(foodOrderItems);
        beveragesLiveData.setValue(beverageOrderItems);
    }

    @Override
    public void onAdd(String itemName) {
        foodOrderItems.stream().filter(orderItem -> orderItem.getItemName().equals(itemName)).findFirst().ifPresent(OrderItem::add);
        beverageOrderItems.stream().filter(orderItem -> orderItem.getItemName().equals(itemName)).findFirst().ifPresent(OrderItem::add);
        refreshItems();
    }

    @Override
    public void onRemove(String itemName) {
        foodOrderItems.stream().filter(orderItem -> orderItem.getItemName().equals(itemName)).findFirst().ifPresent(OrderItem::remove);
        beverageOrderItems.stream().filter(orderItem -> orderItem.getItemName().equals(itemName)).findFirst().ifPresent(OrderItem::remove);
        refreshItems();
    }

    public void sendOrder() {
        AtomicReference<Double> price = new AtomicReference<>(0.0);
        String itemsString = foodOrderItems.toString().concat(beverageOrderItems.toString());
        Stream.concat(foodOrderItems.stream(), beverageOrderItems.stream()).forEach(
                orderItem -> price.updateAndGet(p -> p + orderItem.getQuantity() * orderItem.getItemPrice())
        );

        placeOrderRepository.sendOrder(3, itemsString, price.get(), getDate());
    }

    void clearOrder() {
        order.clear();
        foodOrderItems.clear();
        beverageOrderItems.clear();
        refreshItems();
    }

    LiveData<List<OrderItem>> getFoodOrderItems() {
        return dishesLiveData;
    }

    LiveData<List<OrderItem>> getBeverageOrderItem() {
        return beveragesLiveData;
    }

    LiveData<PlaceOrderResponse> getPlaceOrderResponseMutableLiveData() {
        return placeOrderRepository.getPlaceOrderResponseMutableLiveData();
    }

    private void refreshItems() {
        dishesLiveData.setValue(foodOrderItems);
        beveragesLiveData.setValue(beverageOrderItems);
    }

    private List<OrderItem> convertToOrderItem(List<? extends Item> dishes) {
        Set<OrderItem> orderItems = new HashSet<>();
        dishes.forEach(dish -> {
            int count = Collections.frequency(dishes, dish);
            orderItems.add(new OrderItem(dish, count));
        });

        return new ArrayList<>(orderItems);
    }

    private String getDate() {
        DateFormat.getDateInstance();
        DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format(new Date());
    }
}