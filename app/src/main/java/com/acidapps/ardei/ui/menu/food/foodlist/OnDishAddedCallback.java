package com.acidapps.ardei.ui.menu.food.foodlist;

import com.acidapps.ardei.model.dish.Dish;

public interface OnDishAddedCallback {
    void onAdd(Dish dish);
}
