package com.acidapps.ardei.ui.menu.beverage.beveragelist;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.acidapps.ardei.model.order.Order;
import com.acidapps.ardei.model.beverage.Beverage;
import com.acidapps.ardei.model.category.beverage.BeverageCategory;
import com.acidapps.ardei.ui.ArdeiApp;

import java.util.ArrayList;
import java.util.List;

public class BeverageListViewModel extends AndroidViewModel implements OnBeverageAddedCallback {
    private ArdeiApp ardeiApp;
    private Order order;

    BeverageListViewModel(@NonNull ArdeiApp application, Order order) {
        super(application);

        this.ardeiApp = application;
        this.order = order;
    }

    public List<Beverage> getBeverages(String categoryName) {
        List<Beverage> beverages = new ArrayList<>();
        for (BeverageCategory category : ardeiApp.getBeverageCategories()) {
            if (categoryName.equals(category.getName())) {
                beverages = category.getBeverages();
            }
        }
        return beverages;
    }

    @Override
    public void onAdd(Beverage beverage) {
        order.addBeverage(beverage);
    }
}
