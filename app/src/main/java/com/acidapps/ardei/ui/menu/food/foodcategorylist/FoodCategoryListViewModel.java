package com.acidapps.ardei.ui.menu.food.foodcategorylist;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.acidapps.ardei.ui.ArdeiApp;
import com.acidapps.ardei.model.category.food.FoodCategory;

import java.util.List;

public class FoodCategoryListViewModel extends AndroidViewModel {
    public FoodCategoryListViewModel(@NonNull Application application) {
        super(application);
    }

    List<FoodCategory> getFoodCategories() {
        return ((ArdeiApp) getApplication()).getFoodCategories();
    }
}
