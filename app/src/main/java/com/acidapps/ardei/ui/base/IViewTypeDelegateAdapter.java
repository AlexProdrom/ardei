package com.acidapps.ardei.ui.base;

import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.model.base.IViewType;

public interface IViewTypeDelegateAdapter {
    RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    void onBindViewHolder(RecyclerView.ViewHolder holder, IViewType viewType);
}
