package com.acidapps.ardei.ui.menu.food.foodlist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.DishItemBinding;
import com.acidapps.ardei.model.dish.Dish;

import java.util.ArrayList;
import java.util.List;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.ViewHolder> {

    private OnDishAddedCallback onDishAddedCallback;
    private List<Dish> dishes = new ArrayList<>();

    DishAdapter(OnDishAddedCallback onDishAddedCallback) {
        this.onDishAddedCallback = onDishAddedCallback;
    }

    void setDishes(List<Dish> dishes) {
        if (this.dishes.isEmpty()) {
            this.dishes = dishes;
            notifyItemRangeInserted(0, dishes.size());
        } else {
            //TODO: background
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return DishAdapter.this.dishes.size();
                }

                @Override
                public int getNewListSize() {
                    return dishes.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return DishAdapter.this.dishes.get(oldItemPosition).getName().equals(dishes.get(newItemPosition).getName());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Dish newProduct = dishes.get(newItemPosition);
                    Dish oldProduct = DishAdapter.this.dishes.get(oldItemPosition);
                    return newProduct.getName().equals(oldProduct.getName())
                            && newProduct.getPrice() == oldProduct.getPrice();
                }
            });
            this.dishes = dishes;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DishItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.dish_item,
                parent,
                false);
        binding.setAddDishCallback(onDishAddedCallback);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setDish(dishes.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final DishItemBinding binding;

        ViewHolder(DishItemBinding dishItemBinding) {
            super(dishItemBinding.getRoot());
            binding = dishItemBinding;
        }
    }
}
