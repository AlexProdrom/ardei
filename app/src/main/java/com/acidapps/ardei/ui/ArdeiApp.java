package com.acidapps.ardei.ui;

import android.app.Application;

import com.acidapps.ardei.model.Menu;
import com.acidapps.ardei.model.category.beverage.BeverageCategory;
import com.acidapps.ardei.model.category.food.FoodCategory;

import java.util.List;

public class ArdeiApp extends Application implements IArdeiApp {
    private Menu mMenu;

    @Override
    public List<FoodCategory> getFoodCategories() {
        return mMenu.getFoodCategories();
    }

    @Override
    public List<BeverageCategory> getBeverageCategories() {
        return mMenu.getBeverageCategories();
    }

    public void setMenu(Menu menu) {
        mMenu = menu;
    }
}
