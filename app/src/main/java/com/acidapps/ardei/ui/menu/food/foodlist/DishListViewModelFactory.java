package com.acidapps.ardei.ui.menu.food.foodlist;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.acidapps.ardei.model.order.Order;
import com.acidapps.ardei.ui.ArdeiApp;

public class DishListViewModelFactory implements ViewModelProvider.Factory {
    private ArdeiApp application;
    private Order order;

    DishListViewModelFactory(ArdeiApp application, Order order) {
        this.application = application;
        this.order = order;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DishListViewModel(application, order);
    }
}
