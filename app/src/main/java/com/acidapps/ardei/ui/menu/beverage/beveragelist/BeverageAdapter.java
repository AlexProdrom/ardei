package com.acidapps.ardei.ui.menu.beverage.beveragelist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.acidapps.ardei.R;
import com.acidapps.ardei.databinding.BeverageItemBinding;
import com.acidapps.ardei.model.beverage.Beverage;

import java.util.ArrayList;
import java.util.List;

public class BeverageAdapter extends RecyclerView.Adapter<BeverageAdapter.ViewHolder> {
    private OnBeverageAddedCallback onBeverageAddedCallback;
    private List<Beverage> beverages = new ArrayList<>();

    BeverageAdapter(OnBeverageAddedCallback onBeverageAddedCallback) {
        this.onBeverageAddedCallback = onBeverageAddedCallback;
    }

    void setBeverages(List<Beverage> beverages) {
        if (beverages.isEmpty()) {
            this.beverages = beverages;
            notifyItemRangeInserted(0, beverages.size());
        } else {
            //TODO: background
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return BeverageAdapter.this.beverages.size();
                }

                @Override
                public int getNewListSize() {
                    return beverages.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return BeverageAdapter.this.beverages.get(oldItemPosition).getName().equals(beverages.get(newItemPosition).getName());
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    Beverage newBeverage = beverages.get(newItemPosition);
                    Beverage oldBeverage = BeverageAdapter.this.beverages.get(oldItemPosition);
                    return newBeverage.getName().equals(oldBeverage.getName())
                            && newBeverage.getPrice() == oldBeverage.getPrice();
                }
            });
            this.beverages = beverages;
            result.dispatchUpdatesTo(this);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BeverageItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.beverage_item,
                parent,
                false);
        binding.setAddBeverageCallback(onBeverageAddedCallback);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setBeverage(beverages.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return beverages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final BeverageItemBinding binding;

        ViewHolder(@NonNull BeverageItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
