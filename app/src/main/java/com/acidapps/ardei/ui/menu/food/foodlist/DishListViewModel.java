package com.acidapps.ardei.ui.menu.food.foodlist;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.acidapps.ardei.model.order.Order;
import com.acidapps.ardei.model.category.food.FoodCategory;
import com.acidapps.ardei.model.dish.Dish;
import com.acidapps.ardei.ui.ArdeiApp;

import java.util.ArrayList;
import java.util.List;

public class DishListViewModel extends AndroidViewModel implements OnDishAddedCallback {
    private ArdeiApp ardeiApp;
    private Order order;

    DishListViewModel(@NonNull ArdeiApp application, Order order) {
        super(application);

        this.ardeiApp = application;
        this.order = order;
    }

    public List<Dish> getDishes(String categoryName) {
        List<Dish> dishes = new ArrayList<>();
        for (FoodCategory category : ardeiApp.getFoodCategories()) {
            if (categoryName.equals(category.getName())) {
                dishes = category.getDishes();
            }
        }
        return dishes;
    }

    @Override
    public void onAdd(Dish dish) {
        order.addDish(dish);
    }
}
