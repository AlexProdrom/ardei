package com.acidapps.ardei.ui.menu.beverage.beveragecategorylist;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.acidapps.ardei.model.category.beverage.BeverageCategory;
import com.acidapps.ardei.ui.ArdeiApp;

import java.util.List;

public class BeverageCategoryListViewModel extends AndroidViewModel {
    public BeverageCategoryListViewModel(@NonNull Application application) {
        super(application);
    }

    List<BeverageCategory> getBeverageCategories() {
        return ((ArdeiApp) getApplication()).getBeverageCategories();
    }
}
