package com.acidapps.ardei;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws UnknownHostException {

        System.out.println(InetAddress.getLocalHost());
        assertEquals(4, 2 + 2);
    }
}